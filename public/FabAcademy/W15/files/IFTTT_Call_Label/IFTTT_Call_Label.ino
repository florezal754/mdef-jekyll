#include <IFTTTMaker.h>

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

char ssid[] = "IAAC-WIFI";       // your network SSID (name)
char password[] = "enteriaac2013";  // your network key

#define KEY "nb0HS__fbdHPUts2clq1HNZEHfiitiy9Dy-KqDRULaz"  // Get it from this page https://ifttt.com/services/maker/settings
#define EVENT_NAME "call_label"



int led = 14;     // LED pin D5
int sensorbutton = A0; // push button is connected
int temp = 0;    // temporary variable for reading the button pin status
IPAddress ip;
WiFiClientSecure client;
IFTTTMaker ifttt(KEY, client);

void setup() {
  pinMode(D5, OUTPUT);   // declare LED as output
  pinMode(A0, INPUT); // declare push button as input
  Serial.begin(115200);

  // Set WiFi to station mode and disconnect from an AP if it was Previously
  // connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  // Attempt to connect to Wifi network:
  Serial.print("Connecting Wifi: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("Llegando");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  ip = WiFi.localIP();
  Serial.println(ip);

}
void loop() {
  int adcState = analogRead(A0);   // read the input on analog pin 0
  Serial.println(adcState);

  // code section for the button
  temp = analogRead(sensorbutton);

  if (adcState > 5) {
    digitalWrite(led, HIGH);
    Serial.println("LED Turned ON");
    //triggerEvent takes an Event Name and then you can optional pass in up to 3 extra Strings


    if (ifttt.triggerEvent(EVENT_NAME, "call_label")) {
      Serial.println("Successfully sent");
      delay(3000);
    } else
    {
      Serial.println("Failed!");
    }
    delay(60000);
  } else {
    digitalWrite(led, LOW);
    Serial.println("LED Turned OFF");
    delay(1000);
  }

}
