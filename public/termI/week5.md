Abstract
> 

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

![](images/17.JPG)

**How do I know that I am lost?** Instead of responding myself I start to asking myself. This response leaded me to a reflection. Paul Ansina encorauged my thought and I got to understand that designing is a force that is usually driven by answers instead of questions. His talk was such as a space to re-visit the designing process, the way we design and conceive the enrivonment, how everything is being affected by the design that it has behind. Design is a force that has to be used excepting on consequences. This stirred something inside me: the importance of a phylosophical aproach in every discipline to analyze human experience. The society needs to be asked.

“We don't answer answers, we ask questions. How do we make our voices heard? Is design more powerful than we think?   -Anthony Burril's Workshop

Understand the time and the space that we are living in. Get to know the society needs, ask them what are they looking for or waiting for. Maria Acaso points out in her TED's talk about how to change the landscape of education that “the enery not only has to be focused in what we transfer (knowledge), communicate or express but also in the architecture of its transmission”. She established five microrevolutions that, from my perspective, can be extrapolated to other discipline such as design. In the end, educating is no more than designing ways of learning for the alumni.
“Focus the energy in the understanding of HOW? Instead of focusing in the WHAT!”

![](images/18.JPG)
![](images/19.JPG)


As proffesionals that are creating something new, something that will be involved with the people, it will have its impact, and it is crucial to think in advance about it. It will have not only impact in people's life, but also in the environment we are living in, in our planet. And our planet's needs must stay in the center of every designer decission. We can't change what already happened, we can just design the future.  Jose Luis Vicente  pointed out some of his thoughts trying to asnwer the question: What can we do as desginers? Althought it was something that all of us we already knew, having the chance to meet him and listeling to his ideas made me think from another pesrpective in the way everything it's being designed and the place that designers take. Rethink about the design decissions that we are making right now. Service design and system design, impelemting new models. Designing interventions, problem dissolving, interface of people with experience, redesign the experience we have with everythig.

Why? Because we as designers have the opportunity to make this happen. From every action we take now will depend how we will reach the world of the latter half of the 21st century. How the society's responsibility to the generations who will be born and grow up in it.

**Emergent futures**: interfaces that measure the way we are acting as we are doing. Make people ask themselves: what's the impact of individual actions?/ Data visualization of Scale and size of the problem. Recognition, action, actors, generators. ‘Not giving plastic bags will end up with the problem’/ Open sources: no matter how you use it or get involved with it, but how can you do something and contribute

The real change comes when you try to change paradigmas     []    How to drive in the next economy?


Our brain is about or is already experimenting a new era, a new stage. It is used to exchange of information. Surrounded of multiple stimulus. The question though is: What is conciousness? When is the human being concious? When we had the opportunity to have Nuria Conde teaching us what means being concious. She pointed out the explanation with a simple example: when we are talking about human being we know when we are concious; there is a point when you are a baby that you realize that you have fingers in your hands; here the human being become realizing about something that he didn't know before. That is when you start having a voice, to tell your hand to move the fingers, you start having that conciousness. Your concoius awakes. If something becomes concious, how do you know? Because you start having a moral thing to do, concious became an ACTIVE PLAYER.

Evolution  is being such in a way that it is changing the course of civilization. The human being will be concious when he becames an ACTIVE PLAYER.  What will be the human been able to experiment in the future? New interfaces, new technologies, another civilization. How can the human senses be extended? The human is in the center of a huge system. How can we create new human senses? The future of technology is creating new senses for humans that allows them to amplify their cognitiion and their conciousness by experimenting new signals.

**Emergent futures:** new senses for humans/new   systems that allows people to receive the particulas that are invisible to the brain in a way of experimenting and also fixing


![](images/20.JPG)
![](images/21.JPG)
![](images/22.JPG)

Having said that, I would like to INVESTIGATE and EXPLORE the connections between four different fields in which I want to take an active part and help something to evolve.

![](images/Captura.JPG)

**WEARABLES**
 What fascinates me about the wearable's world is its industry which is having to start a dialogue surrounding it's impact on the environment and also to humans. I had always a huge interest in the fashion industry and I have been always wondering how it will evolve, how it will take place in the future. What to do? Help the fashion industry to evolve and create a new landscape. The zero point will be starting to talk about wearables instead of fashion. Create wearables capables of compite with the actual fashion industry and innovate with new approaches and perspectives. Everything is about meaningful moves in this industry. Approach it from a realistic point of view and world's environment. Change the way we buy fashion, the value we are giving to it, destroy big companies marketing strategies by facing them with new ways of buying fashion and being part of it. Key workds:  belonging [] powerful [] accesible [] flexible [] inclusion [] rights based [] atraction Key sententes: accesible to every customer [] fashion that reflects and represents who is wearing it [] close to natural things and meaningful
Jonathan Kyle Farmer strablished the approach that I would like to follow in the following steps that I can take in this industry. He points out that fashion (wearables) research follows the process of a washing machine system:

![](images/23.JPG)

One of his works called ''drawing with scissors'' is a simple way of representing the future of wearables. The way he analizes the body and plays with it is fascinating. ''If clothes make the man and the man make the clothes, is fashion a Palindrome?'' In this work he explores the 5D dimension and plays with the tehcnology as a part of the creation. This is such a great example of what I would like to do in termns of exploration and interaction between humans and wearables. His research is based in practical explorations that leads him to the next step. His work is based in a way of researching when he is open to accidents that happens when you are exploring and the opportunity that the accident gives to you to identify new actions and reactions. He digitalizes fashion and animate it.

PROCESS DRIVEN: process thought  – playing – exploring

At FIT he is the creator and visionary behind a new and unique MFA Fashion Design program centered around 6 core words/values/concepts: EXPERIENCE,IDEA,PLAY,FOCUS,EDIT,CONCLUDE He is such an innovator in the way we concieve fashion and how it evolves. He is being researching for years about the conection between craft and technology, where he developed a concept called craftology. His research focuses on the idea of craft:OLOGY, which asks: Is technology contrary to craft, or rather one of craft's vital components¿ Is technology the tool that evolves fashion, and craft what embodies the emotions?
He is an inspirational curator for me as the way he take the process into a new era of contemplating the future of fashion, the designing system and the educational system in this field. My GOAL is to have my process driven as he does.
practice

Interventions
- Open sources in Wearables
Open ecosystem that allows the society to reuse the knowledege. Accesible and flexible. Fabrication through new technologies as the proyect 'Leaders' in which her creator explores the way of fabrication through 3D printer. I am interested in the way she is approaching it to reduce the waste in fabrication processes. The ethics and transparence behind it are key values that drive my ideas of intervention.
- Re-frame the laws of wearables: Definition of Law ''the principles and regulations established in a community by some authority and applicable to its people, whether in the form of legislation or of custom and policies recognized and enforced by judicial decision.'' Who is the authority in the fashion industry? Which are the policies of designing?
Facing the current policies installed in the wearable's industry and create an interface, a system that allows amplifies the cognition of the potential customer. Make information availabe about how the wearables are produced and which are the skateholders that are interveining in the process. For instance, create virtual reality labels that shows the information from every wearable. Suistanable economy, ecological marketing and suistanable wearables are some of the approaches to archieve with this intervention. Make the people be an ACTIVE PLAYER in the way of cosuming. Make the people answer themselves which are the impacts of every individual action. If people take part and start being concious about what they are consuming there is a chance to re-frame the laws of wearables.
- Body architecture: How can technology transform the human body? Explore how can we transform the human body with the wearables we create.

**EDUCATION + COMMUNITIE**
Development of communities with proper education and interactions + Increase knowledge of poor communities to evolve by themselves
Solutions for undevelopment countries such as Africa: Solutions for them based in giving them the knowledge to grow and stop creating economy from the outside. Open a fablab system in the community of Mombasa and teach them how to maintain themselves installing creating a system based in circular economy. A system based in local manufacturing where they can learn how to reuse the materials that they have available in their town.

![](images/24.JPG)

Highlight the knowledge of LEARNING BY DOING, creating OPEN SOURCES to: – enable emerging makers – gather resources and tools – learning by doing and from others – produce better productions – trade and generate – amplyfly maker's potentials
Design a new language in education: how is it possible that we are still following the same patterns of a hundred years ago? Key points of a new language: Interaction and communication between educational levels: Since we are part of the educational system our knowledge is rarely used or developed. Schools, high schools and universities are key places where the transmition of ideas, thoughts and knowledge happen. Interaction is barely done between the students from different levels. For instance, most of the research that people who are attending to high school make usually is never going to be developed or to be taken further. It evaporates and goes to some repository that the institutions have or either to the bin.  I propose a CIRCULAR KNOWLEDGE where it finds a wider path to be transformed and developed in such as many different NEW things. COOPERATION.

![](images/25.JPG)

Waking up the activity of both hemispheres of the brain: Since we are born society and the environment teach us how to use the left hemisphere and there is a chance to improve people's behavior by teaching how to awake the right hemisphere. How to combine them to get to better solutions (whatever a solution means).



Five microrevolution system: Make every microrevolution mentioned above happen

![](images/26.JPG)
