Abstract
> through a week based in hands-on we have learnt how mix the old with the new in relation to tooling, techniques and material resources & the ephemeral life of projects and the life cycle of it. The idea of the studio we have created  is to maintain its essence but not its form. It can be changed  as  many  times  as needed and most of it is made out of modular layouts.  There is a second life for some of the productions but also all of them can be modified, extended or re-created.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.


Creating “new objetcs/something” with a zero-waste perspective.  We went out several times and collect the trash that the citiziens from Barcelona[1] throw away. Collecting discarded objetcs in the neighborhood at the marked points[2] was a grateful experience for all of us. Having the chance to   grab all of this valuable material for our future space was meaningful as all of it will be filling in our classroom once is transformed into “our way”.

**waste collected = valuable set of materials**

For a truly suistanable world, we need zero waste productions[4]; reconsidering the second life of every object and material is crucial to maintain the planet. It is important to reconsider the use of every thing we throw away and instead of thinking about it as a waste thinking it as a future valuable objetc that can be re-designed, extended, re-created and, of course, reused. The aim of this zero waste productions  is to use all the collected materials and create something new without spending money in buying something else. What are we able to make with the things we have available? This is the question we had to answer, the challenge we had.  As designers reconsidering the second life of every material was a challenge.

**material collected = material available**

The real challenge was thinking how can digital fabrication tools could be used in a creative way to work with the resources that we had. Most of us had never worked before with these techniques, machines and tools so it turned into a pretty intense week where we had to explore all of these new posibilities to turning our ideas into reality. Hacking the collected objects into new ones to create our new environment and space was quite exciting, it was going to be all for us, for our day a day ''in our own studio''.

**material avaialbe + digital production/local manufacturing = new creations**

Summing up, it was a very intense week where we put so much effort into creating a functional and elegant studio. Personally I believe that there is a huge gap between the digital fabrication tools and techniques and my practice, I do need to keep learning as I don't consider I got the necessary knowledge to do it independently. It was a succesful week in termns of hours spent and work done, we need to keep working into some of the designs and test the various scenarious that our space present.


. . . . . . . . . . . . .

n o t e s

Our space is there waiting for us to be designed and modified just to take it to the maximum potential. We have the opportunity to turn the existing problems and needs of the space into huge and epic solutions, efficient solutions that allow us to drive into our Master’s degree program.

**Key points**

- Valuable material is waiting for us to be used and transformed
- New productions will be created and will be lovable objects
- Creativity is there to be found. A mix of brains can get to a way of work with these resources
- A multidisciplinary approach and view can create huge things
- The importance of the rules behind the digital fabrication
- Local manufacturing through design

#Introduction, Design and Conceptualisation
First thoughts
Work space  []  Study space  []  Presentation space  []  Meeting point & Break space [] Classes space  []  Green space

Current problems into opportunities
No ventilation   []   Poor Light   []  No working space   []  No chilling space  
[]  Waste of money (coffee & water)  []  Presentations   []  Placement

WORK SPACE
options[storage]      cozy[charis]      modular[tables]      flexibility[moving spaces]
CHILLING SPACE
Cofee&Water[reduce waste]     cooperation&collective[library&sofa]     meeting point[conversation area]    breaks[relax]
PRESENTATION SPACE
Good quality of use      flexible      easy to use
GREEN SPACE
Comfortable      Breathing      Brain  

Key words:
1) ILLUMINATION
Terrace (We need light, no natural light at all)    []       Class (curtains out  roller blinds synthetic

2) VENTILATION
Fans []  Air circulation

3) OPTIMIZATION
Spaces separation (work, fab lab, study, presentation, chilling)

4) STRUCTURATION
Classroom’s uses change []  Re-structure  [] Creating new spaces

5) CREATION
Personalize & Customize

Uses of the existing materials
WOOD  []  SYNTHETIC TEXTILES  []  PLASTICS
Big creations  []  Modules  []  Storage  []  Tables  []  Chairs  []  Wall + Library  []  Columns

SOME SKETCHING...

![](images/sket.JPG)



SOLVING THE PROBLEM OF LIGHT...

![](images/light.JPG)

BIG ONES  light points:   INSIDE   []   OUTSIDE (roof)
1) inside: MIRRORS  [material we already have → 0 waste]  


reduce the Waste of electricity                       
increase Natural light     

#Digital fabrication and presentation

![](images/1.JPG)
![](images/2.JPG)
![](images/3.JPG)
![](images/4.JPG)


From paper to prototype to production

![](images/5.JPG)
![](images/6.JPG)
![](images/7.JPG)
![](images/8.JPG)
![](images/9.JPG)
![](images/10.JPG)
![](images/11.JPG)
![](images/12.JPG)

**Thoughts that came up during the week**
The question though is:  How can I develop this ideas and turn them into reality?
The future implementation of these thoughts...  to be continued
